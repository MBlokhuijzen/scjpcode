package nl.mblokhuijzen.scjptests.initblocks;

/**
 * @author MBlokhuijzen
 * @since 16-11-2013 - 10:47 AM
 */
public class SubClass extends SuperClass {

    {
        System.out.println("Init in Sub");
    }

    static {
        System.out.println("Static in Sub");
    }

    public SubClass() {
        System.out.println("Constructor in Sub");
    }
}
