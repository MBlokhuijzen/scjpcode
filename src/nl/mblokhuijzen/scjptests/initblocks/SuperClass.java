package nl.mblokhuijzen.scjptests.initblocks;

/**
 * @author MBlokhuijzen
 * @since 16-11-2013 - 10:47 AM
 */
public class SuperClass {

    {
        System.out.println("Init in Super");
    }

    static {
        System.out.println("Static in Super");
    }

    public SuperClass() {
        System.out.println("Constructor in Super");
    }
}
